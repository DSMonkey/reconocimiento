//
//  ViewController.swift
//  Reconocimiento de voz
//
//  Created by MacbookProDSM on 8/8/17.
//  Copyright © 2017 Jesus Navarrete Perez. All rights reserved.
//

import UIKit
import Speech


class ViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    var audioRecordingSession : AVAudioSession! // Creamos una variable de tipo AVAudioSession
    
    let audioFileName : String = "audio-recordered.m4a" // creo una variable para guardar el audio grabado que se guardara con este monbre.m4a
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Llamo a la función de reconocimiento de voz
        RecognizeSpeech()//Llamo al metodo de reconocimiento que he creado
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Logica de la funcion de reconocimiento de voz
    func RecognizeSpeech(){
        
        //Pido autorizacion al ususario y si me la da ejecuto el bloque if y si no le else que escribe un mensaje en la consola.
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            
            if authStatus == SFSpeechRecognizerAuthorizationStatus.authorized{
                
                //Mediante un casting seguro busco en el path principal del programa el archivo de llamado "audio" con extension "mp3"
                if let urlPath = Bundle.main.url(forResource: "audio", withExtension: "mp3"){
                    
                    //ahora creo un reconocedor 
                    let recognizer = SFSpeechRecognizer()
                    //Ahora necesitamos una peticion de reconocimiento del archivo que esta el la urlPath
                    let request = SFSpeechURLRecognitionRequest(url: urlPath)
                    
                    //ejecutamos la tarea de reconocimiento con el "request" anteriormente creado y nos puede devolver dos cosas el resultado del reconocimiento que yo he llamado "result" o un error que yo he llamado "error"
                    recognizer?.recognitionTask(with: request, resultHandler: { (result, error) in
                        // Para comprobar si ha habido un error casteo el error con sigo mismo y si lo puede castear es que existe asi que lo imprimo por consola.
                        if let error = error {
                            
                            print("Algo a salido mal: \(error.localizedDescription)")
                            
                        }
                        else{
                            // utilizo la pabra reservada Self porque estoy dentro de un bloque de completacion "Es para referencias objetos que estan fuera del bloque de la funcion donde se ejecuta el codigo."
                            //Meto el resultado en la textView.text pero la tengo que formatear a string.
                            //1) result.bestTranscription.formattedString
                            //2) Entre parentesis y String para convertirlo todo
                            self.textView.text = String(describing: result?.bestTranscription.formattedString)
                            
                        }
                        
                    })
                }
                
                
            }else{
                print("No tienes autorización.")
            }
            
            
        }
        func recordingAudioSetup(){
            audioRecordingSession = AVAudioSession.sharedInstance()// inicializamos la variable nada mas lanzar el metodo
            
            do {
                //configuramos la sesion
                try audioRecordingSession.setCategory(AVAudioSessionCategoryRecord)// Configuramos la categoria
                try audioRecordingSession.setActive(true)// activamos la sesion
                
                //pedimos permisos al usuario
                audioRecordingSession.requestRecordPermission({ [unowned self] (allowed:Bool) in
                    if allowed {
                        //Espezamos a grabar.
                    }else{
                        print(("Necesito permisos"))
                    }
                })
                
                
            }
            catch{
                
                print("Ha habido algun error al configurar el audio")
            }
            
        }
        
        func directoryUrl() -> NSURL? {
            
            let fileManager = FileManager.default
            let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
            let documentDirectory = urls[0] as URL
            do{
            return try documentDirectory.appendingPathComponent(audioFileName) as NSURL
            }catch{
                print ("No hemos podido crear la carpeta")
            }
            return nil
        }
    }

}

